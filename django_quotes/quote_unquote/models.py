from django.db import models

# Create your models here.

class Quote(models.Model):
    saying = models.CharField(max_length=40)
    def __unicode__(self):
        return str(self.id) + "   " + self.saying
        
