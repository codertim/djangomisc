from django.conf.urls.defaults import patterns, url

from quote_unquote import views

urlpatterns = patterns('',
    url(r'^$',     views.index,   name='index'),
    url(r'^list$', views.list,    name='list_quotes'),
)
