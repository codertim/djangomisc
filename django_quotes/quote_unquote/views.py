# Create your views here.

from quote_unquote.models import Quote

from django.http import HttpResponse
from django.template import Context, loader


def index(request):
  output = ""
  quote_count = Quote.objects.count()
  output += "Quote count: " + str(quote_count) + "<br /><br /><br />"
  output += "<a href='list'>List Quotes</a>"
  my_template = loader.get_template("quotes/index.html")
  # return HttpResponse(output)
  context = Context({
    'quote_count': str(quote_count),
  })
  return HttpResponse(my_template.render(context))


def list(request):
  output = ""
  quotes = Quote.objects.all()
  output += "<h3>All Quotes</h3>"
  output += "<br /><br />"

  for q in quotes:
    output += q.saying
    output += "<br />"

  # return HttpResponse(output)
  my_template = loader.get_template("quotes/list_template.html")
  context = Context({
    'all_quotes': quotes, 
  })
  return HttpResponse(my_template.render(context))


