from django.conf.urls.defaults import patterns, url

from stringy import views

urlpatterns = patterns('',
    url(r'^$',         views.index,     name='index'),
	url(r'^case',      views.case,      name='case'),
	url(r'^inputCase', views.inputCase, name='input-case'),
	url(r'^getsize',   views.getsize,   name='get-string-size'),
	url(r'^size',      views.size,      name='string-size')
)
