# Create your views here.
from django.http import HttpResponse

def index(request):
	output = ""
	output += "<html><head><title>Django String Utilities</title></head><body>"
	output += "<br />"
	output += "<h2>String Utilities</h2>"
	output += "<br />"
	output += "<br />"
	output += "<a href='size'>String Size</a>"
	output += "<br />"
	output += "<br />"
	output += "<a href='inputCase'>Make string upper-case</a>"
	output += "</body></html>"
	return HttpResponse(output)



def case(request):
	data = request.POST
	input_string = data['inputstring']
	output_string = input_string.upper()
	output = ""
	output += "<html><head><title>Case</title></head><body>"
	output += "<br />"
	output += "<br />"
	output += "Upper case string: %s" % (output_string)
	output += "</body></html>"
	return HttpResponse(output)



def inputCase(request):
	output = ""
	output += "<html><head><title>String Case</title></head><body>"
	output += "  <br />"
	output += "  <br />"
	output += "  Enter String: <br />"
	output += "  <br />"
	output += "  <form method='post' action='case'>"
	output += "    <input name='inputstring' size='90' />"
	output += "    <br />"
	output += "    <br />"
	output += "    <input type='submit' value='Upper Case' />"
	output += "  </form>"
	output += "</body></html>"
	return HttpResponse(output)



def getsize(request):
	data = request.POST
	# print "POST data = ", data

	input_string = data['inputstring']
	string_size = len(input_string)

	my_output = ""
	my_output += "<html><head><title>String Size</title></head><body>"
	my_output += "   <br /><br /><br />"
	my_output += "  You entered |%s|" % input_string
	my_output += "   <br /><br /><br />"
	my_output += "  Size: %s " %  str(string_size)
	my_output += "</body></html>"
	return HttpResponse(my_output)


def size(request):
	my_output = ""
	my_output += "<html><head><title>String Size</title></head><body>"
	my_output += "  <br />"
	my_output += "  <br />"
	my_output += "  Enter String: <br />"
	my_output += "  <br />"
	my_output += "  <form method='post' action='getsize'>"
	my_output += "    <textarea name='inputstring' cols='100' rows='10' >"
	my_output += "    </textarea>"
	my_output += "    <br />"
	my_output += "    <br />"
	my_output += "    <input type='submit' value='Go' />"
	my_output += "  </form>"
	my_output += "</body></html>"
	return HttpResponse(my_output)



